>>> from price.price_service import PriceService
>>> 
>>> ps = PriceService()
>>> 
>>> ps.setPackSize('A')
>>> ps.price()
5
>>> ps.setPackSize('B')
>>> ps.price()
10
>>> ps.setPackSize('C')
>>> ps.price()
15
