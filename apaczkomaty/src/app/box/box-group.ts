import { Box } from './box';
import { MachineVisitor } from '../machine/machine-visitor';
import { PackSize } from '../pack/pack-size';

export class BoxGroup {
    boxes: Box[];
    sizeMappings: {[key in PackSize]: PackSize[]} = {
        A: [PackSize.A, PackSize.B, PackSize.C],
        B: [PackSize.B, PackSize.C],
        C: [PackSize.C]
    };

    getSmallestEmptyBox(packSize: PackSize): Box {
        let available = this.sizeMappings[packSize];
        for (let size of available) {
            const candidate = this.boxes.filter(box => box.size == size);
            if (candidate.length > 0) {
                for (let box of candidate) {
                    if (box.isEmpty()) {
                        return box;
                    }
                }
            }
        }

        return null;
    }

    accept(visitor: MachineVisitor) {
        visitor.visitBoxGroup(this);
        this.boxes.forEach(box => box.accept(visitor));
    }
}