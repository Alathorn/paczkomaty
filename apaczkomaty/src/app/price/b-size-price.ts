import { SizePriceStrategy } from './size-price-strategy'

export class BSizePrice implements SizePriceStrategy {
    
    price(): number {
        return 10;
    }
}