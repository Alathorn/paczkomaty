import { Injectable } from '@angular/core';
import { PackSize } from '../pack/pack-size';
import { SizePriceStrategy } from './size-price-strategy';
import { ASizePrice } from './a-size-price';
import { BSizePrice } from './b-size-price';
import { CSizePrice } from './c-size-price';

@Injectable()
export class PriceStrategyFactory {
    getPriceStrategy(packSize: PackSize): SizePriceStrategy {
        throw new Error('Unexpected price');
    }
}