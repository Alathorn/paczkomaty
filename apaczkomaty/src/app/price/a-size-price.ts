import { SizePriceStrategy } from './size-price-strategy'

export class ASizePrice implements SizePriceStrategy {
    
    price(): number {
        return 5;
    }
}