import { Component } from '@angular/core';
import { PackSize } from '../pack/pack-size';
import { PriceService } from './price-service';
import { Subject } from 'rxjs';
import { PriceSelectorComponent } from './price-selector.component';

@Component({
    selector: 'prices',
    template: `
        <form>
            <price-selector (packSize)="onItemSelected($event)"></price-selector>
            <input type="button" (click)="onCalculate()" value="Oblicz">
            <input type="text" [value]="priceSubject | async">
        </form>
    `
})
export class PricesComponent {
    priceSubject = new Subject<number>();

    constructor(private priceService: PriceService) {
    }

    onItemSelected(item: PackSize): void {
        this.priceService.setPackSize(item);
    }

    onCalculate(): void {
        this.priceSubject.next(this.priceService.price());
    }
}