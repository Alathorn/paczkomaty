export interface SizePriceStrategy {
    price(): number;
}