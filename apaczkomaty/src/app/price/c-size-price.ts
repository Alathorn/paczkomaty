import { SizePriceStrategy } from './size-price-strategy'

export class CSizePrice implements SizePriceStrategy {
    
    price(): number {
        return 15;
    }
}