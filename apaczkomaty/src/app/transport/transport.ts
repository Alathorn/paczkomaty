import { Pack } from '../pack/pack';

export class Transport {
    place: string;

    constructor(private _id: string,
                private _pack: Pack,
                private _destination: string) {}

    get pack(): Pack {
        return this._pack;
    }

    get id(): string {
        return this._id;
    }

    get destination(): string {
        return this._destination;
    }
}