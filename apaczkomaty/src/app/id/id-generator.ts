export abstract class IdGenerator {
    abstract getPackId(): string;
    abstract getPackMachineId(): string;
    abstract getTransportId(): string;
}