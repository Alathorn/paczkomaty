import { Component } from '@angular/core';
import { IdGenerator } from './id-generator';

@Component({
    selector: 'test-id',
    template: `
        <div class="module">
            <button (click)="generatePack()">Paczka</button>
            <button (click)="generateTransport()">Transport</button>
            <button (click)="generatePackMachine()">Paczkomat</button>
        </div>
        <div>
            <span>{{ id }}</span>
        </div>
    `
})
export class IdComponent {
    id: string;

    constructor(private idGenerator: IdGenerator) {}

    generatePack(): void {
        this.id = this.idGenerator.getPackId();
    }

    generateTransport(): void {
        this.id = this.idGenerator.getTransportId();
    }

    generatePackMachine(): void {
        this.id = this.idGenerator.getPackMachineId();
    }
}