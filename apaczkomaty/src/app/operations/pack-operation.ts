export interface PackOperation {
    execute(): void;
}