import { PackOperation } from './pack-operation';
import { Transport } from '../transport/transport';
import { PackMachine } from '../machine/pack-machine';

export class SendPackOperation implements PackOperation {
    
    constructor(private transport: Transport,
                private packMachine: PackMachine) {}

    execute(): void {
    }
    
}