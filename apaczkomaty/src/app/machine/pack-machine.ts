import { BoxGroup } from '../box/box-group';
import { MachineVisitor } from './machine-visitor';
import { Pack } from '../pack/pack';

export class PackMachine {
    private groups: BoxGroup[] = [];

    constructor(private _id: string) {}

    get id(): string {
        return this._id;
    }

    put(pack: Pack): void {
        for (let group of this.groups) {
            let box = group.getSmallestEmptyBox(pack.size);
            if (box != null) {
                box.put(pack);
                return;
            }
        }
    }

    add(boxGroup: BoxGroup): void {
        this.groups.push(boxGroup);
    }

    clear(): void {
        this.groups = [];
    }

    accept(visitor: MachineVisitor) {
        visitor.visitPackMachine(this);
        this.groups.forEach(group => group.accept(visitor));
    }
}