import { Component } from '@angular/core';
import { BoxGroupBuilder } from '../box/box-group-builder';
import { PackMachine } from './pack-machine';
import { IdGenerator } from '../id/id-generator';
import { MachineQuantitiesComponent } from './machine-quantities.component';
import { PackOperationsComponent } from './pack-operations.component';
import { Transport } from '../transport/transport';
import { SendPackOperation } from '../operations/send-pack-operation';

@Component({
    selector: 'machines',
    template: `
        <div class="module">
            <label>Liczba boksów A</label>
            <input #numberOfAInput type="text" [value]="numberOfA" (keyup)="setA(numberOfAInput.value)">
            <label>Liczba boksów B</label>
            <input #numberOfBInput type="text" [value]="numberOfB" (keyup)="setB(numberOfBInput.value)">
            <label>Liczba boksów C</label>
            <input #numberOfCInput type="text" [value]="numberOfC" (keyup)="setC(numberOfCInput.value)">
            <input type="button" value="Zbuduj paczkomat" (click)="build()">
        </div>
        <div class="module">
            <div>Model:</div>
            <textarea>{{ asJson(packMachine) }}</textarea>
        </div>
        <quantities [packMachine]="packMachine"></quantities>
        <pack-operations [packMachine]="packMachine" (sendTransport)="sendPack($event)"></pack-operations>
    `
})
export class MachinesComponent {
    numberOfA: number = 10;
    numberOfB: number = 4;
    numberOfC: number = 2;
    packMachine: PackMachine = new PackMachine(this.idGenerator.getPackMachineId());

    constructor(private idGenerator: IdGenerator) { }

    build(): void {
        const builder: BoxGroupBuilder = new BoxGroupBuilder();
        this.packMachine.clear();
        this.packMachine.add(builder.withA(this.numberOfA).withB(this.numberOfB).withC(this.numberOfC).build());
    }

    asJson(object: any): string {
        return JSON.stringify(object);
    }

    setA(value: string): void {
        this.numberOfA = Number(value);
    }

    setB(value: string): void {
        this.numberOfB = Number(value);
    }

    setC(value: string): void {
        this.numberOfC = Number(value);
    }

    sendPack(tr: Transport): void {
        const operation = new SendPackOperation(tr, this.packMachine);
        operation.execute();
    }
}
