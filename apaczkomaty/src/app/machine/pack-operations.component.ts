import { Component, Input, Output } from '@angular/core';
import { PackMachine } from './pack-machine';
import { PriceSelectorComponent } from '../price/price-selector.component';
import { PackSize } from '../pack/pack-size';
import { Pack } from '../pack/pack';
import { Transport } from '../transport/transport';
import { IdGenerator } from '../id/id-generator';
import { SendPackOperation } from '../operations/send-pack-operation';
import { Subject } from 'rxjs';

@Component({
    selector: 'pack-operations',
    template: `
    <div class="module">
        <div>Nadanie paczki</div>
        <price-selector (packSize)="onSizeSelected($event)"></price-selector>

        <label>Paczkomat docelowy</label>
        <input #destMachine (keyup)="setDestinationMachine(destMachine.value)">
        <input type="button" value="Utwórz paczkę" (click)="createPack(destMachine.value)">
        <input type="button" value="Nadaj paczkę" (click)="sendPack()">
        <div>Transport</div>
        <textarea>{{ asJson(transport) }}</textarea>
    </div>
    `
})
export class PackOperationsComponent {

    @Input()
    packMachine: PackMachine;

    @Output()
    sendTransport: Subject<Transport> = new Subject();

    pack: Pack;
    transport: Transport;
    destination: string;
    packSize: PackSize;

    constructor(private idGenerator: IdGenerator) {}

    asJson(object: any): string {
        return JSON.stringify(object);
    }

    onSizeSelected(item: PackSize): void {
        this.packSize = item;
    }

    createPack(destination: string): void {
        this.pack = new Pack(this.idGenerator.getPackId(), this.packSize);
        this.transport = new Transport(this.idGenerator.getTransportId(), this.pack, this.destination);
    }

    sendPack(): void {
        this.sendTransport.next(this.transport);
    }

    setDestinationMachine(destination: string): void {
        this.destination = destination;
    }


}