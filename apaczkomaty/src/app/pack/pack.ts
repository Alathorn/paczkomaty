import { PackSize } from './pack-size';

export class Pack {
    constructor(private _id: string,
                private _packSize: PackSize) { }

    get id(): string {
        return this._id;
    }

    get size(): PackSize {
        return this._packSize;
    }
}